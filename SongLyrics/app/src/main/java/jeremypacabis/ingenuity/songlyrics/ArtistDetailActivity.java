package jeremypacabis.ingenuity.songlyrics;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by Jeremy Patrick on 8/11/2017.
 * Author: Jeremy Patrick G. Pacabis
 * for jeremypacabis.ingenuity.songlyrics @ SongLyrics
 */

public class ArtistDetailActivity extends AppCompatActivity {
    private TextView ad_name, ad_bname, ad_bdate, ad_age;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.artist_details);
        Artist artist = (Artist) getIntent().getSerializableExtra("the_artist");

        ad_name = (TextView) findViewById(R.id.ad_name);
        ad_bname = (TextView) findViewById(R.id.ad_bname);
        ad_bdate = (TextView) findViewById(R.id.ad_bdate);
        ad_age = (TextView) findViewById(R.id.ad_age);

        ad_name.setText(artist.getName());
        ad_bname.setText(artist.getBirth_name());
        ad_bdate.setText(artist.getBirth_date());
        ad_age.setText(artist.getAge());

        setTitle("About " + artist.getName());
    }
}
