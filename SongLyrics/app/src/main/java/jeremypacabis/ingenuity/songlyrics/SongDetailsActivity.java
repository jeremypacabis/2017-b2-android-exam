package jeremypacabis.ingenuity.songlyrics;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by Jeremy Patrick on 8/11/2017.
 * Author: Jeremy Patrick G. Pacabis
 * for jeremypacabis.ingenuity.songlyrics @ SongLyrics
 */

public class SongDetailsActivity extends AppCompatActivity {
    private TextView sd_title, sd_artist, sd_producer, sd_lyrics, sd_album;
    private Button btn_edit;
    private EditText etxt_lyrics;
    private boolean edit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.song_details);
        edit = false;
        Song song = (Song) getIntent().getSerializableExtra("the_song");
        final Artist artist = (Artist) getIntent().getSerializableExtra("the_artist");
        setTitle(song.getTitle() + " by " + song.getArtist());
        sd_title = (TextView) findViewById(R.id.sd_title);
        sd_artist = (TextView) findViewById(R.id.sd_artist);
        sd_producer = (TextView) findViewById(R.id.sd_producer);
        sd_lyrics = (TextView) findViewById(R.id.sd_lyrics);
        sd_album = (TextView) findViewById(R.id.sd_album);
        btn_edit = (Button) findViewById(R.id.btn_edit);
        etxt_lyrics = (EditText) findViewById(R.id.sd_edit_lyrics);

        sd_title.setText(song.getTitle());
        sd_artist.setText(song.getArtist());
        sd_producer.setText(song.getProducer());
        sd_album.setText(song.getAlbum());
        sd_lyrics.setText(song.getLyrics());

        sd_artist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent openArtistDetailsIntent = new Intent(getApplicationContext(), ArtistDetailActivity.class);
                openArtistDetailsIntent.putExtra("the_artist", artist);
                startActivity(openArtistDetailsIntent);
            }
        });

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!edit) {
                    sd_lyrics.setVisibility(View.GONE);
                    etxt_lyrics.setVisibility(View.VISIBLE);
                    etxt_lyrics.setText(sd_lyrics.getText().toString());
                } else {
                    sd_lyrics.setVisibility(View.VISIBLE);
                    etxt_lyrics.setVisibility(View.GONE);
                    sd_lyrics.setText(etxt_lyrics.getText().toString());
                }

                edit = !edit;
                btn_edit.setBackgroundResource(edit ? android.R.drawable.ic_menu_save : android.R.drawable.ic_menu_edit);
            }
        });
    }
}
