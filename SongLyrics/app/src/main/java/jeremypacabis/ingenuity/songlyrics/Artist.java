package jeremypacabis.ingenuity.songlyrics;

import java.io.Serializable;

/**
 * Created by Jeremy Patrick on 8/11/2017.
 * Author: Jeremy Patrick G. Pacabis
 * for jeremypacabis.ingenuity.songlyrics @ SongLyrics
 */

public class Artist  implements Serializable {
    private String name, birth_name, birth_date, age, key;

    public Artist(String key, String name, String birth_date, String birth_name, String age) {
        this.age = age;
        this.key = key;
        this.name = name;
        this.birth_date = birth_date;
        this.birth_name = birth_name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getAge() {
        return age;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public String getBirth_name() {
        return birth_name;
    }

    public String getName() {
        return name;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public void setBirth_name(String birth_name) {
        this.birth_name = birth_name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
