package jeremypacabis.ingenuity.songlyrics;

import java.io.Serializable;

/**
 * Created by Jeremy Patrick on 8/11/2017.
 * Author: Jeremy Patrick G. Pacabis
 * for jeremypacabis.ingenuity.songlyrics @ SongLyrics
 */

public class Song implements Serializable{
    private String title, artist, album, producer, lyrics;

    public Song(String title, String artist, String album, String producer, String lyrics) {
        this.title = title;
        this.artist = artist;
        this.album = album;
        this.producer = producer;
        this.lyrics = lyrics;
    }

    public String getProducer() {
        return producer;
    }

    public String getTitle() {
        return title;
    }

    public String getAlbum() {
        return album;
    }

    public String getArtist() {
        return artist;
    }

    public String getLyrics() {
        return lyrics;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
