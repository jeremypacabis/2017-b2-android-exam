package jeremypacabis.ingenuity.songlyrics;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView songListRecyclerView;
    private Spinner artist_spinner;
    private SongsAdapter songsAdapter;
    private List<JSONObject> songsList;
    private List<Song> songs;
    private List<Artist> artists;
    private String[] artists_spinner_items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        artist_spinner = (Spinner) findViewById(R.id.artist_spinner);

        songsList = new ArrayList<>();
        songs = new ArrayList<>();
        artists = new ArrayList<>();
        songListRecyclerView = (RecyclerView) findViewById(R.id.songs_list_recycler_view);
        songsAdapter = new SongsAdapter(songsList);
        songListRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        songListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        songListRecyclerView.setAdapter(songsAdapter);
        songListRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Log.d("Clicked: ", "Position: " + position);
                try {
                    Intent openSongDetailsIntent = new Intent(getApplicationContext(), SongDetailsActivity.class);
                    openSongDetailsIntent.putExtra("the_song", getSong(songsList.get(position).getString("title")));
                    openSongDetailsIntent.putExtra("the_artist", getArtist(songsList.get(position).getString("artist")));
                    startActivity(openSongDetailsIntent);
                } catch (JSONException jse) {

                }

            }
        }));

        setSpinnerListener();
        getSongsData();
    }

    private void setSpinnerListener() {
        artist_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                songsList = new ArrayList<>();

                if (position == 0) {
                    resetSongsList();
                } else {
                    String artist = artists.get(position - 1).getName();
                    for (Song song : songs) {
                        if (song.getArtist().equals(artist)) {
                            try {
                                songsList.add(
                                        new JSONObject(
                                                "{" +
                                                        "\"title\":\"" + song.getTitle() + "\"," +
                                                        "\"artist\":\"" + song.getArtist() + "\"" +
                                                        "}"
                                        )
                                );
                            } catch (JSONException jse) {
                                Log.e("JSON exception: ", jse.toString());
                            }
                        }
                    }
                }

                songListRecyclerView.setAdapter(new SongsAdapter(songsList));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getSongsData() {
        JSONArray songsArray, artistsArray;
        try {
            String str;
            StringBuilder builder = new StringBuilder();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(getApplicationContext().getResources().openRawResource(R.raw.songs)));
            while ((str = bufferedReader.readLine()) != null) {
                builder.append(str);
            }

            songsArray = new JSONArray(builder.toString());
            builder = new StringBuilder();
            bufferedReader = new BufferedReader(new InputStreamReader(getApplicationContext().getResources().openRawResource(R.raw.artists)));
            while ((str = bufferedReader.readLine()) != null) {
                builder.append(str);
            }

            artistsArray = new JSONArray(builder.toString());

            Log.d("songs JSONArr", songsArray.toString());
            Log.d("artists JSONArr", artistsArray.toString());

            artists_spinner_items = new String[artistsArray.length() + 1];
            artists_spinner_items[0] = "All Artists";

            for (int x = 0; x < artistsArray.length(); x++) {
                JSONObject artist = artistsArray.getJSONObject(x);
                JSONObject artist_details = artist.getJSONObject("fields");

                artists.add(new Artist(
                        artist.getString("pk"),
                        artist_details.getString("name"),
                        artist_details.getString("birth_name"),
                        artist_details.getString("birth_date"),
                        artist_details.getString("age")
                ));
            }

            Collections.sort(artists, new Comparator<Artist>() {
                public int compare(Artist a1, Artist a2) {
                    return a1.getName().compareTo(a2.getName());
                }
            });

            for (int x = 0; x < songsArray.length(); x++) {
                JSONObject song = songsArray.getJSONObject(x);
                JSONObject song_details = song.getJSONObject("fields");

                songs.add(new Song(
                        song_details.getString("title"),
                        getArtistWithPk(song_details.getString("artist")),
                        song_details.getString("album"),
                        song_details.getString("producer"),
                        song_details.getString("lyrics")
                ));
            }

            resetSongsList();

            for (int x = 0; x < artists.size(); x++) {
                artists_spinner_items[x + 1] = artists.get(x).getName();
            }

            artist_spinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, artists_spinner_items));

            songsAdapter.notifyDataSetChanged();
        } catch (JSONException jse) {
            Log.d("JSON error", jse.toString());
        } catch (IOException ioe) {
            Log.d("IO except", ioe.toString());
        }

    }

    private String getArtistWithPk(String pk) {
        for (Artist artist : artists) {
            if (artist.getKey().equals(pk)) {
                return artist.getName();
            }
        }

        return "Unknown artist";
    }

    private Song getSong(String title) {
        for (Song song : songs) {
            if (song.getTitle().equals(title)) {
                return song;
            }
        }

        return null;
    }

    private Artist getArtist(String name) {
        for (Artist artist : artists) {
            if (artist.getName().equals(name)) {
                return artist;
            }
        }

        return null;
    }

    public void resetSongsList() {
        Collections.sort(songs, new Comparator<Song>() {
            public int compare(Song s1, Song s2) {
                return s1.getTitle().compareTo(s2.getTitle());
            }
        });

        try {
            for (Song song : songs) {
                songsList.add(
                        new JSONObject(
                                "{" +
                                        "\"title\":\"" + song.getTitle() + "\"," +
                                        "\"artist\":\"" + song.getArtist() + "\"" +
                                        "}"
                        )
                );
            }
        } catch (JSONException jse) {

        }
    }
}
