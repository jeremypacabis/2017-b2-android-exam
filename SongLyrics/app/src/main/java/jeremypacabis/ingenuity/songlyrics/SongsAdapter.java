package jeremypacabis.ingenuity.songlyrics;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Jeremy Patrick on 8/11/2017.
 * Author: Jeremy Patrick G. Pacabis
 * for jeremypacabis.ingenuity.songlyrics @ SongLyrics
 */

public class SongsAdapter extends RecyclerView.Adapter<SongsAdapter.MyViewHolder> {

    private List<JSONObject> songsList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView song_title, song_artist;

        public MyViewHolder(View view) {
            super(view);
            song_title = (TextView) view.findViewById(R.id.song_title);
            song_artist = (TextView) view.findViewById(R.id.song_artist);
        }
    }

    public SongsAdapter(List<JSONObject> songsList) {
        this.songsList = songsList;
    }

    @Override
    public SongsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.song_list_row, parent, false);
        return new MyViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(SongsAdapter.MyViewHolder holder, int position) {
        JSONObject song = songsList.get(position);
        try {
            holder.song_title.setText(song.getString("title"));
            holder.song_artist.setText(song.getString("artist"));
        } catch (JSONException jse) {
            Log.e("JSON error", jse.toString());
        }
    }

    @Override
    public int getItemCount() {
        return songsList.size();
    }
}
